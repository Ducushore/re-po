from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView

from RepoPortalAPP import models


class XlsUploadView(CreateView):
    # permission_required = 'products.can_put_on_sale'
    model = models.XlsFileUpload
    template_name = 'xls_add.html'
    fields = '__all__'
    success_url = reverse_lazy('xls_list')


class XlsListView(ListView):
    model = models.XlsFileUpload
    template_name = 'xls_list.html'
    context_object_name = 'all_xls_uploads'
