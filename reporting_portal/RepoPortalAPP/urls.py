from django.urls import path

from RepoPortalAPP import views


urlpatterns = [
    path('xls_upload', views.XlsUploadView.as_view(), name='xls_upload'),
    path('xls_list', views.XlsListView.as_view(), name='xls_list'),

]
